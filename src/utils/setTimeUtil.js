const UtilSetTime = {
    sleep: (millis) => new Promise((resolve => setTimeout(resolve, millis)))
}

export default UtilSetTime;