import React, { useEffect, useState,createContext } from 'react';
import './App.css';
import Auth from './components/Auth'
import Card from './components/Card'
import Login from './components/Login';
import Signup from './components/Signup';
import Restaurantreview from './components/Restaurantreview'
import Welcome from './components/Welcome';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from './components/Home';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navbarmain from './components/Navbarmain';

export const RoleControlContext = createContext()

function App() {

  const [theUser, setTheUser] = useState(null)
  //const [loading, setLoading] = useState(true);

  const changeUser =(user)=>{
    setTheUser(user)
  }
  useEffect(() => {
    
    const user = sessionStorage.getItem('user')
    
    if (user!==null) {
      setTheUser(JSON.parse(user))
    }
    
  }, [])

  /* return  loading ? <></> : */
  return (
    <BrowserRouter>
    <RoleControlContext.Provider value={{theUser, changeUser}}>
      <Navbarmain />
      <ToastContainer autoClose={1000} />
      
      
      
      <Routes>
        {theUser!==null ? 
        <>
          <Route path="/" element={<Welcome />} />
          <Route path="/Home" element={<Home />} />
          <Route path="/Restaurantreview/:id" element={<Restaurantreview />} />
        </>
        : 
        <>
          <Route path="/" element={<Welcome />} />
          <Route path="/Login" element={<Login />} />
          <Route path="/Signup" element={<Signup />} />
        </>
        }
        </Routes>
      </RoleControlContext.Provider>
      

    </BrowserRouter>
  )
  
}

export default App;
