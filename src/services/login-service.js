import UtilSetTime from "../utils/setTimeUtil";

const LoginService = {
    fetchLoggedInUserDetails: async (user) => {
        await UtilSetTime.sleep(2000)
        // 20% of the time the function will throw error
        /* if (Math.random() <  0.20 ) throw new Error("Unable to fetch logged in user details"); */

        // 50% of the time logged in user details would be admin and 50% of time his role will be regular
        if(user==='Admin'){
            const data = {
                name: 'Random Admin 1',
                token:12345,
                role: 'admin'
            }
            /* sessionStorage.setItem('token', data.token);
            sessionStorage.setItem('role', data.role); */
            return {
                data
            }
        }
        else{
            const data = {
                name: 'Random User 1',
                token:67890,
                role: 'user'
            }
            /* sessionStorage.setItem('token', data.token);
            sessionStorage.setItem('role', data.role); */
            return {
                data
            }
        }
        
    }
}

export default LoginService;