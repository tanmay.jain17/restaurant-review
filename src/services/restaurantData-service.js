import UtilSetTime from "../utils/setTimeUtil";
import data from '../restaurantsdata.json';
const restaurantsdataService = {
    fetchRestaurants: async () => {
        await UtilSetTime.sleep(2000)
        
        return data
    },
    fetchSingleRestaurant: async (id)=>{
        await UtilSetTime.sleep(2000)
        //let theRestaurant
        for(let i of data){
            console.log(i.id,'id',id)
            if(i.id==id){
                console.log(i)
                return i;
            }
            /* if(i.id===id){
                console.log(i)
                return i
            } */
        }
        /* console.log(theRestaurant)
        return theRestaurant */
    },

    saveRestaurantEdits:async(edits)=>{
        await UtilSetTime.sleep(2000)

        return edits
    },
    deleteRestaurant:async(data,index)=>{
        await UtilSetTime.sleep(2000)
        data = data.filter(theRest=>theRest.id!==index)
        return data
    }
}


export default restaurantsdataService;