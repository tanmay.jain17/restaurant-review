import React, { useEffect, useState, useContext } from 'react';
import Rating from "@mui/material/Rating";
import { RoleControlContext } from '../App';
import { useLocation, useSearchParams, useParams } from 'react-router-dom'
import restaurantsdataService from "../services/restaurantData-service";

function Restaurantreview() {
    
    const [isEditable, setIsEditable] = useState(false)
    const [editForm, setEditForm] = useState({ name: false, location: false })

    const [theRestaurant, setTheRestaurant] = useState({})
   
   const [theRes,setTheRes] = useState({})
    
    const { theUser } = useContext(RoleControlContext)
    const { id } = useParams()


    useEffect(() => {

        restaurantsdataService.fetchSingleRestaurant(id)
            .then((response) => {
                console.log(response)
                setTheRestaurant(response)
                setTheRes(response)
                
            })
        console.log(theUser)

    }, [id, theUser])

    const canEditRestaurant = () => {
        setIsEditable(true)
    }

    const enableEditForm = (elementName) => {

        elementName === 'name' ? setEditForm({ ...editForm, name: true, location: false }) : setEditForm({ ...editForm, location: true, name: false })
    }

   

    const handleFormChange = (e,elementName) => {
        elementName === 'name' ? setTheRestaurant({ ...theRestaurant, name: e.target.value }) : setTheRestaurant({ ...theRestaurant, location: e.target.value })
    }

    const handleEditChanges = (e, elementName) => {

        if (elementName === 'name') {
            if (!/\S/.test(theRestaurant.name)) {
                setTheRestaurant({ ...theRestaurant, name: theRestaurant.name })
            }
            else if (theRestaurant.name === '') {
                setTheRestaurant({ ...theRestaurant, name: theRestaurant.name })
            }
            else {
                setTheRestaurant({ ...theRestaurant, name: theRestaurant.name })
            }
            setEditForm({ ...editForm, name: false, location: false })
        }
        else {
            if (!/\S/.test(theRestaurant.location)) {
                setTheRestaurant({ ...theRestaurant, location: theRestaurant.location })
            }
            else if (theRestaurant.location === '') {
                setTheRestaurant({ ...theRestaurant, location: theRestaurant.location })
            }
            else {
                setTheRestaurant({ ...theRestaurant, location: theRestaurant.location })
            }
            setEditForm({ ...editForm, name: false, location: false })
        }



    }

    const saveRestaurantEdits = ()=>{
        setTheRes({...theRes,name:theRestaurant.name,location:theRestaurant.location})
        restaurantsdataService.saveRestaurantEdits(theRestaurant)
        .then((response)=>{
            console.log(response)
            
            setIsEditable(false)
        })
    }

    const cancelRestaurantEdits = ()=>{
        setTheRestaurant({ ...theRestaurant, name: theRes.name,location: theRes.location})
        setIsEditable(false)
    }

    
    return (
        <><h1>Restaurantreview</h1>
            <h1>{theRestaurant.name}</h1>
            {theUser.role === 'admin' ? <>
                {isEditable ? <>
                    <div key={theRestaurant.id} /* onClick={disableEditForm} */ className="card App">
                        <img src={theRestaurant.img} className="card-img-top" alt="..." />
                        <div className="card-body">

                            {(!editForm.name) && <h5 name='name' onClick={(e) => enableEditForm('name')} className="card-title">{theRestaurant.id} {theRestaurant.name}</h5>}

                            {(editForm.name) &&
                                <form onSubmit={(e) => handleEditChanges(e, 'name')}>
                                    <input
                                        className="formInput"
                                        style={{ padding: "5px 5px 5px 5px", fontSize: "16px" }}
                                        type="text"
                                        onBlur={() => handleEditChanges('name')}
                                        onChange={(e) => handleFormChange(e, 'name')}
                                        autofocus
                                    />
                                    <button type="submit" style={{ display: "none" }}></button>
                                </form>}

                            {(!editForm.location) && <p onClick={(e) => enableEditForm('location')} className="card-text">{theRestaurant.location}</p>}

                            {(editForm.location) &&
                                <form onSubmit={(e) => handleEditChanges(e, 'location')}>
                                    <input
                                        className="formInput"
                                        style={{ padding: "5px 5px 5px 5px", fontSize: "16px" }}
                                        type="text"
                                        onBlur={() => handleEditChanges('location')}
                                        onChange={(e) => handleFormChange(e, 'location')}
                                        autofocus
                                    />
                                    <button type="submit" style={{ display: "none" }}></button>
                                </form>}

                            <a onClick={saveRestaurantEdits} href="#" className="btn btn-success">SAVE</a>
                            <a onClick={cancelRestaurantEdits} href="#" className="btn btn-outline-danger">CANCEL</a>
                            <Rating
                                style={{ position: 'relative', marginTop: '3px', marginLeft: '3px' }}
                                name="simple-controlled"
                                value={theRestaurant.rating}
                                readOnly
                            /* onChange={(event, newValue) => {
                              handleRating(newValue);
                            }} */
                            />
                        </div>

                    </div>
                </> : <>
                    <div  className="card App">
                        <img src={theRestaurant.img} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">{theRestaurant.id} {theRestaurant.name}</h5>
                            <p className="card-text"> {theRestaurant.location}</p>

                            <a onClick={canEditRestaurant} href="#" className="btn btn-warning">EDIT</a>

                            {/* <a  style={{ position: 'relative', marginLeft: '5px' }} href="#" className="btn btn-danger">DELETE</a> */}

                            <Rating
                                style={{ position: 'relative', marginTop: '3px', marginLeft: '3px' }}
                                name="simple-controlled"
                                value={theRestaurant.rating}
                                readOnly
                            /* onChange={(event, newValue) => {
                              handleRating(newValue);
                            }} */
                            />
                        </div>

                    </div>
                </>}
            </> : <>
                <div className="card App">
                    <img src={theRestaurant.img} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{theRestaurant.id} {theRestaurant.name}</h5>
                        <p className="card-text">{theRestaurant.location}</p>


                        <Rating
                            style={{ position: 'relative', marginTop: '3px', marginLeft: '3px' }}
                            name="simple-controlled"
                            value={theRestaurant.rating}
                            readOnly

                        />
                    </div>

                </div>
            </>}

        </>

    )
}

export default Restaurantreview