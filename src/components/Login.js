import React, { useContext, useState } from "react";
import { NavLink} from "react-router-dom";
import LoginService from "../services/login-service";
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import '../main.css'
import { Grid, Paper, Avatar, Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import TextField from '@mui/material/TextField';
import { RoleControlContext } from "../App";


export default function Login() {
  const initialValues = { email: "", password: "" };
  const [formValues, setFromValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  let navigate = useNavigate();
  const {theUser, changeUser} = useContext(RoleControlContext);





  const handleChange = (e) => {
    const { name, value } = e.target;
    setFromValues({ ...formValues, [name]: value });
  }
  const handleSubmit = async (e) => {
    e.preventDefault();
    const errors = await validate(formValues)
    if (errors.email || errors.password) {
      setFormErrors(errors)
    }
    else {
      try {
        LoginService.fetchLoggedInUserDetails('Admin')
          .then((response) => {
            console.log(response.data)
            const {name,token,role} = response.data;
            /* var user = {'name':'John'}; */
            const obj = {'name':name,'token':token,'role':role}
            sessionStorage.setItem('user', JSON.stringify(obj));
            /* var obj = JSON.parse(sessionStorage.user); */
            
            /* sessionStorage.setItem('user', obj.stringify) */
            changeUser(response.data)
            toast("Wow login Success")
            navigate("/Home");


          })
      }
      catch (error) {
        console.log(error)
      }
    }

  }


  const validate = async (values) => {

    const errors = {};
    const regex1 = /^[a-zA-Z0-9]+@+[a-zA-Z0-9]+.+[A-z]/;
    const regex2 = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/

    if (!values.email) {
      errors.email = "Email is required!";
    } else if (!regex1.test(values.email)) {
      errors.email = "This is not valid";
    }
    if (!regex2.test(values.password)) {
      errors.password = "Password must follow the rule";
    }
    return errors;
  };








  const paperStyle = {
    background: 'whitesmoke'
    , textAlign: 'center', borderRadius: '4rem',
    border: "solid {0}", padding: '36px', margin: '60px 32.5vw', display: 'inline-block',
    width: '35%', marginTop: '3rem',
  }

  const avtarstyle = { backgroundcolor: 'black' }
  const btnstyle = { margin: '8px 0' }
  const forg = { margin: '20px 50px' }


  return (
    <Grid>
      <Paper elevation={10} style={{ background: 'lightsteelblue', }}>



        <form onSubmit={handleSubmit} style={paperStyle}>
          <Grid align="center">
            <Avatar style={avtarstyle}>{ }</Avatar>
            <h2>Sign In</h2>
          </Grid>

          <div>
            <TextField

              label="Email ID:"
              id="outlined-size-small"
              size="small"
              name="email"
              value={formValues.email} onChange={handleChange}
              autoComplete="off"
              autoFocus
            />
          </div><br />
          <div>
            <p className="message">{formErrors.email}</p>
          </div>
          <div className="passwordTextfeild">
            <TextField
              type="password"
              label="Password:"
              id="outlined-size-small"
              size="small"
              name="password"
              value={formValues.password} onChange={handleChange}
              autoComplete="off"
            />
            <span className="passInfo">i</span>
            <span style={{ fontSize: "10px", color: "red" }} className="passRequirements">
              (1)Must be atleat 6 characters long
              (2)Must contain atleat 1 Uppercase letter
              (3)Must contain atleat 1 lowercase letter
              (4)Must contain 1 special symbol
            </span>
          </div>
          <div>
            <p className="message">{formErrors.password}</p>
          </div>


          <Button type='submit' color='primary' variant="contained"
            style={btnstyle} >
            Sign In</Button>


          <Typography style={forg}> Do you have any account:
            <NavLink to="/Signup">
              Sign Up
            </NavLink>
          </Typography>
        </form>


      </Paper>
    </Grid>
  )
}


