import Rating from "@mui/material/Rating";
import React, { useState, useContext } from 'react';
import {useNavigate} from 'react-router-dom'
import { RoleControlContext } from '../App'
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import restaurantsdataService from "../services/restaurantData-service";
import '../main.css';

export default function RestaurantCard({ theRestaurant,deleteFunction}) {
    console.log(theRestaurant)
    const [isEditable, setIsEditable] = useState(false)
    const [editForm, setEditForm] = useState({ name: false, location: false })
    
    const [theRes,setTheRes] = useState({name:theRestaurant.name,location:theRestaurant.location})
    const [restaurantValues, setRestaurantValues] = useState({ name: theRestaurant.name, location: theRestaurant.location })
    const [rrating, setRrating] = useState(0)
    const { theUser } = useContext(RoleControlContext)
    let navigate = useNavigate();

    const handleRatingChange = (e) => {
        setRrating(e.target.value)
    }

    const canEditRestaurant = () => {
        setIsEditable(true)
    }

    const enableEditForm = (elementName) => {

        elementName === 'name' ? setEditForm({ ...editForm, name: true, location: false }) : setEditForm({ ...editForm, location: true, name: false })
    }

   

    const handleFormChange = (e,/* theItem, */elementName) => {
        elementName === 'name' ? setRestaurantValues({ ...restaurantValues, name: e.target.value }) : setRestaurantValues({ ...restaurantValues, location: e.target.value })
    }

    const handleEditChanges = (e, elementName) => {

        if (elementName === 'name') {
            if (!/\S/.test(restaurantValues.name)) {
                setRestaurantValues({ ...restaurantValues, name: theRestaurant.name })
            }
            else if (restaurantValues.name === '') {
                setRestaurantValues({ ...restaurantValues, name: theRestaurant.name })
            }
            else {
                setRestaurantValues({ ...restaurantValues, name: restaurantValues.name })
            }
            setEditForm({ ...editForm, name: false, location: false })
        }
        else {
            if (!/\S/.test(restaurantValues.location)) {
                setRestaurantValues({ ...restaurantValues, location: theRestaurant.location })
            }
            else if (restaurantValues.location === '') {
                setRestaurantValues({ ...restaurantValues, location: theRestaurant.location })
            }
            else {
                setRestaurantValues({ ...restaurantValues, location: restaurantValues.location })
            }
            setEditForm({ ...editForm, name: false, location: false })
        }



    }

    const saveRestaurantEdits = ()=>{
        setTheRes({...theRes,name:restaurantValues.name,location:restaurantValues.location})
        restaurantsdataService.saveRestaurantEdits(restaurantValues)
        .then((response)=>{
            console.log(response)
            
            setIsEditable(false)
        })
    }

    const cancelRestaurantEdits = ()=>{
       /*  setRestaurantValues({ ...restaurantValues, name: theRestaurant.name,location: theRestaurant.location}) */
        setIsEditable(false)
    }
    const reviewPage = ()=>{
        navigate(`/Restaurantreview/${theRestaurant.id}`)
    }



    return (
        <>
            {theUser.role === 'admin' ? <>
                {isEditable ? <>
                    <div key={theRestaurant.id} /* onClick={disableEditForm} */ className="card App">
                        <img src={theRestaurant.img} className="card-img-top" alt="..." />
                        <div className="card-body">

                            {(!editForm.name) && <h5 name='name' onClick={(e) => enableEditForm('name')} className="card-title">{theRestaurant.id} {restaurantValues.name}</h5>}

                            {(editForm.name) &&
                                <form onSubmit={(e) => handleEditChanges(e,'name')}>
                                    <input
                                        className="formInput"
                                        style={{ padding: "5px 5px 5px 5px", fontSize: "16px" }}
                                        type="text"
                                        onBlur={() => handleEditChanges('name')}
                                        onChange={(e) => handleFormChange(e,'name')}
                                        autofocus
                                    />
                                    <button type="submit" style={{ display: "none" }}></button>
                                </form>}

                            {(!editForm.location) && <p onClick={(e) => enableEditForm('location')} className="card-text">{restaurantValues.location}</p>}

                            {(editForm.location) &&
                                <form onSubmit={(e) => handleEditChanges(e,'location')}>
                                    <input
                                        className="formInput"
                                        style={{ padding: "5px 5px 5px 5px", fontSize: "16px" }}
                                        type="text"
                                        onBlur={() => handleEditChanges('location')}
                                        onChange={(e) => handleFormChange(e,'location')}
                                        autofocus
                                    />
                                    <button type="submit" style={{ display: "none" }}></button>
                                </form>}

                            <a onClick={saveRestaurantEdits} href="#" className="btn btn-success">SAVE</a>
                            <a onClick={cancelRestaurantEdits} href="#" className="btn btn-outline-danger">CANCEL</a>
                            <Rating
                                style={{ position: 'relative', marginTop: '3px', marginLeft: '3px' }}
                                name="simple-controlled"
                                value={theRestaurant.rating}
                                readOnly
                            /* onChange={(event, newValue) => {
                              handleRating(newValue);
                            }} */
                            />
                        </div>

                    </div>
                </> : <>
                    <div  className="card App">
                        <img onClick={reviewPage} src={theRestaurant.img} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">{theRestaurant.id} {theRes.name}</h5>
                            <p className="card-text">{theRes.location}</p>
                            
                            <a onClick={canEditRestaurant} href="#" className="btn btn-warning">EDIT</a>
                            
                            <a onClick={()=>deleteFunction(theRestaurant.id)} style={{ position: 'relative', marginLeft: '5px' }} href="#" className="btn btn-danger">DELETE</a>
                            
                            <Rating
                                style={{ position: 'relative', marginTop: '3px', marginLeft: '3px' }}
                                name="simple-controlled"
                                value={theRestaurant.rating}
                                readOnly
                            /* onChange={(event, newValue) => {
                              handleRating(newValue);
                            }} */
                            />
                        </div>

                    </div>
                </>}

            </> : <>
                <div  className="card App">
                    <img onClick={reviewPage} src={theRestaurant.img} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{theRestaurant.id} {restaurantValues.name}</h5>
                        <p className="card-text">{restaurantValues.location}</p>

                        <FormControl>
                            <InputLabel id="demo-simple-select-label">Rate</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={rrating}
                                onChange={(e) => handleRatingChange(e)}
                                name="userType"
                            >
                                <MenuItem value={1}>1</MenuItem>
                                <MenuItem value={2}>2</MenuItem>
                                <MenuItem value={3}>3</MenuItem>
                                <MenuItem value={4}>4</MenuItem>
                                <MenuItem value={5}>5</MenuItem>
                            </Select></FormControl>
                        <Rating
                            style={{ position: 'relative', marginTop: '3px', marginLeft: '3px' }}
                            name="simple-controlled"
                            value={rrating}
                            readOnly
                        /* onChange={(event, newValue) => {
                          handleRating(newValue);
                        }} */
                        />
                    </div>

                </div>
            </>}

        </>

    );
}
