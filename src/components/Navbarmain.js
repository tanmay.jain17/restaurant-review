import React, { useContext, useEffect, useState } from 'react';
import { RoleControlContext } from '../App';
import rrLogo from '../rrlogo.png';
import { NavLink, useNavigate } from "react-router-dom";
import { Navbar, Nav, Button } from 'react-bootstrap';

export default function Navbarmain() {
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const { theUser }= useContext(RoleControlContext)
    let navigate = useNavigate()
    /* useEffect(()=>{
        if(sessionStorage.getItem('token')){
            setIsLoggedIn(true)
        }
    },[isLoggedIn]) */

    const logOutFunction = () => {
        sessionStorage.removeItem('token')
        navigate("/Login");
    }

    /* const theRole = useContext(RoleControlContext) */
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">
                    <img src={rrLogo} alt="" width="30" height="24" className="d-inline-block align-text-top" />
                    RR
                </a>
                {/* <a className="navbar-brand" href="#">RR</a> */}
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul /* onClick={logOut}  */ className="navbar-nav">
                        {/* {isLoggedIn ? <><li  className="nav-item">

                            Welcome {theRole?.role}

                        </li> <li onClick={logOutFunction} className="nav-item">

                                Log Out

                            </li>
                            <li onClick={logOutFunction} className="nav-item">

                            <NavLink to="/Home">Home</NavLink> 

                        </li></> : <li className="nav-item">

                            <NavLink to="/Login">Log In</NavLink>

                        </li>
                        } */}
                        {theUser ? <><li style={{position: 'relative',marginRight:'0'}} className="nav-item">

                            Welcome-{theUser.name}

                        </li><br/> <li onClick={logOutFunction} className="nav-item">

                                LogOut

                            </li>
                            <br/>
                            <li /* onClick={logOutFunction}  */className="nav-item">

                                <NavLink to="/Home">Home</NavLink>

                            </li></> : <li className="nav-item">

                            <NavLink to="/Login">LogIn</NavLink>

                        </li>
                        }


                    </ul>
                </div>
            </div>
        </nav>


    )
}