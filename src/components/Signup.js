import React, {useEffect, useState} from 'react';
import { Grid, Paper, Avatar, Typography, Button } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';  
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@mui/material/TextField';
import Radium, { StyleRoot } from 'radium';
/* import { StyleRoot } from 'radium'; */

export default function Signup() {
  const initialValues = { userType: "", name: "", email: "", phoneNo: 0, password: "", cnfirmPass: "" }
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);

const useStyles = makeStyles((theme) => ({

    
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }));

  const handleChange = (e) =>{
    const { name, value } = e.target;
    setFormValues({...formValues, [name]: value});
   } 
  const handleSubmit = (e) =>{
    e.preventDefault();
    setFormErrors(validate(formValues));
    setIsSubmit(true);
  };
  useEffect(() => {
    if (Object.keys(formErrors).length === 0 && isSubmit) {
      console.log(formValues);
    }
  }, [formErrors]);
  const validate = (values) => {

    const errors = {};
    const regex = /^[a-zA-Z0-9]+@+[a-zA-Z0-9]+.+[A-z]/;
    if (!values.userType) {
      errors.userType = "Select User type";
    }
    if (!values.email) {
      errors.email = "Email is required!";
    } else if (!regex.test(values.email)) {
      errors.email = "This is not valid";
    }
    
    if (!values.name) {
      errors.name = "name is required";
    }

    if (!values.phoneNo) {
      errors.phoneNo = "mobile number is required!"
    }

    if (!values.password) {
      errors.password = "password is required!";
    } else if (values.password.length < 4) {
      errors.password = "Password must be more than 4 characters";
    }

    if (!values.cnfirmPass) {
      errors.cnfirmPass = "password is required!";
    } else if (values.cnfirmPass.length < 4) {
      errors.cnfirmPass = "Password must be more than 4 characters";
    }


    else if (!(values.cnfirmPass === values.password)) {
      errors.cnfirmPass = "Password did not match";
    }
    return errors;
  };
/* function addUser() {



    setFormValues(formValues)

    axios.post('../../signUp', formValues)

      .then((response) => {
        if (response.data.message) {
          window.alert(response.data.message)
        }
        else {
          if (response.data.id) {
            window.alert(response.data.mess + ' AccountId: ' + (1000 + response.data.id))
            window.location = '/Home'
          }
          else {
            window.alert(response.data.mess)
            window.location = '/Home'
          }

        }
      });
  } */
    
  

   const paperStyle = {background:'whitesmoke'
  ,textAlign:'center',borderRadius: '4rem',
 border:"solid {0}", padding:'20px',margin:'50px 340px', display:'inline-block',
width:'50%',marginTop:'0',

'@media ( max-width:1164px)':{
  textAlign: 'center', borderRadius: '2rem',
border: "solid {0}", padding: '0px', margin: '65px 14rem', display: 'inline-block',
width: '47%',marginTop:'4rem',
 },

 
};
   
    const avatarStyle = { backgroundColor: '#1bbd7e' }
  
    const classes = useStyles();
  
 

    return (
    <Grid>
        <StyleRoot>
      <Paper elevation={20} style={{ background: 'lightsteelblue',padding:'30px',
      marginBottom:'-42px' }}>
        <form onSubmit={handleSubmit} style={paperStyle}>
          <Grid align='center'>
            <Avatar style={avatarStyle}>
            </Avatar>


            <Typography variant='caption' gutterBottom>Please fill this form to create an account !</Typography>
          </Grid>

          <h2 className="letc1" style={{ marginTop: '-1%'}}>Sign Up</h2>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">SELECT ONE</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="userType"
              value={formValues.userType}
              onChange={handleChange}
            >
              <MenuItem value={'Admin'}>Admin</MenuItem>
              <MenuItem value={'User'}>User</MenuItem>
            </Select></FormControl><br />
            <div style={{marginRight: '14rem',}}>
            <p className="message">{formErrors.userType}</p>
            <TextField
              label="Name :"
              id="outlined-size-small"
              defaultValue=""
              size="small"
              /* id="name" */
              name="name"
              values={formValues.name} onChange={handleChange} autoComplete="off"
              required
        
         /> </div> 

<div style={{margin:'30px'}}>
  <p className="message" hidden>{formValues.email}</p>
                 <TextField 
          label="Email :"
          name="email"
          id="outlined-size-small"
          defaultValue=""
          size="small"
          /* id="email" */
          values={formValues.email} onChange={handleChange} autoComplete="off"
           required
       
         />   
             
                 <TextField style={{marginLeft:'6px'}}
            label="Phone Number :"
            id="outlined-size-small"
            defaultValue=""
            size="small"
            name="phoneNo"
            /* id="phoneNo" */
            values={formValues.phoneNo} onChange={handleChange} autoComplete="off"
          />
            <p className="message" >{formValues.phn_number}</p>
              </div>


            
              <div style={{margin:'30px'}}>
              <p className="message" hidden>{formValues.password}</p>
                 <TextField 
       type="password"
       label="Password :"
       name="password"
       id="outlined-size-small"
       defaultValue=""
       size="small"
       /* id="password" */
       values={formValues.password} onChange={handleChange} autoComplete="off" 
         required
         />  
             
                 <TextField style={{marginLeft:'6px'}}
          type="password"
          label="Confirm Password :"
          id="outlined-size-small"
          name="cnfirmPass"
          defaultValue=""
          size="small"
          /* id="name" */
          values={formValues.cnfirmpas} onChange={handleChange} autoComplete="off"
         required
          />
            <p className="message" >{formErrors.cnfirmpas}</p>
              </div>

          <Button /* onClick={addUser} */ type='submit' variant='contained' color='primary'>Sign up</Button>
        </form>
      </Paper>
      </StyleRoot>
    </Grid>
  )
}
