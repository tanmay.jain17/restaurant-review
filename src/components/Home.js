import React, { useEffect, useState,useContext } from 'react';
import { RoleControlContext } from '../App';
import data from '../restaurantsdata.json';
import restaurantsdataService from "../services/restaurantData-service";
import Card from './Card';

export default function Home() {

    const [restData,setRestData] = useState([])
    const { theUser }= useContext(RoleControlContext)

    useEffect(()=>{
       restaurantsdataService.fetchRestaurants()
       .then((response)=>{
         //console.log(response)
         setRestData(response)
       })
    },[])
    console.log("3287678gdua",theUser);

    const deleteRestaurant = (index)=>{
      let restArr = [...restData]
      restaurantsdataService.deleteRestaurant(restArr,index)
      .then((response)=>{
        setRestData(response)
      })
      
      
    }
  
    return (
      <>
      <h1>Home</h1>
      {restData.map((eachRestaurant) =>(
          <Card deleteFunction={deleteRestaurant} key={eachRestaurant.id} theRestaurant={eachRestaurant}/>
      ))}
        
      </>
    
  )
}
